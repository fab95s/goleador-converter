const path = require("path");
const { merge } = require("webpack-merge");

const baseConfig = require("./webpack.config");

module.exports = merge(baseConfig, {
    mode: "development",
    devtool: "source-map",
    devServer: {
        static: {
            directory: path.join(__dirname, "dist")
        },
        host: "localhost",
        hot: true,
        port: 3000
    }
});