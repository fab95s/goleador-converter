const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlPlugin = require("html-webpack-plugin");

module.exports = {
    entry: {
        main: "./src/index.js"
    },
    output: {
        filename: "js/index.js",
        path: path.join(__dirname, "dist"),
        clean: true
    },
    mode: "production",
    module: {
        rules: [
            {
                test: /\.html$/,
                use: {
                    loader: "html-loader"
                }
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(scss)$/,
                exclude: /(node_modules)/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader"
                ]
            }
        ]
    },
    plugins: [
        new HtmlPlugin({
            template: path.join(__dirname, "static/index.html"),
            filename: path.join(__dirname, "dist/index.html")
        }),
        new MiniCssExtractPlugin({
            filename: "css/[name].css",
            chunkFilename: "[id].css",
        })
    ],
    performance: {
        maxEntrypointSize: 300000,
        maxAssetSize: 300000
    }
};