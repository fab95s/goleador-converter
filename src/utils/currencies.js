export const goleador = {
    name: "Goleador",
    iso: "GOL",
    symbol: "⚽",
    eurExchangeRate: 0.10
};

const fixNumber = (value, iso) => value.toFixed(iso === goleador.iso ? 0 : 2);

export const convert = (currencyFrom, currencyTo) => currencyFrom.eurExchangeRate / currencyTo.eurExchangeRate;

export function convertWithFix(currencyFrom, currencyTo) {
    const converted = convert(currencyFrom, currencyTo);
    return fixNumber(converted, currencyTo.iso);
}

export function convertAmount(amount, currencyFrom, currencyTo) {
    const converted = amount * convert(currencyFrom, currencyTo);
    return fixNumber(converted, currencyTo.iso);
}

export default [
    {
        name: "Euro",
        iso: "EUR",
        symbol: "€",
        eurExchangeRate: 1
    },
    {
        name: "US Dollar",
        iso: "USD",
        symbol: "$",
        eurExchangeRate: 0.89628491
    },
    {
        name: "British Pound",
        iso: "GBP",
        symbol: "£",
        eurExchangeRate: 1.202097
    },
    {
        name: "Japanese Yen",
        iso: "JPY",
        symbol: "¥",
        eurExchangeRate: 0.0077621305
    },
    {
        name: "Chinese Yuan Renminbi",
        iso: "CNY",
        symbol: "¥",
        eurExchangeRate: 0.14058305
    },
    {
        name: "Russian Ruble",
        iso: "RUB",
        symbol: "₽",
        eurExchangeRate: 0.011526381
    },
];