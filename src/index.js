import React, { StrictMode } from "react";
import ReactDOM from "react-dom";
import AppComponent from "./components/AppComponent";

window.addEventListener("DOMContentLoaded", () => {
    ReactDOM.render(
        <StrictMode>
            <AppComponent />
        </StrictMode>,
        document.querySelector(".appContainer")
    );
});