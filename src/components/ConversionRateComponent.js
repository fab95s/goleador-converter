import React, { useEffect, useState } from "react";
import { convertWithFix } from "../utils/currencies";

function ConversionRateComponent({ currencyFrom, currencyTo }) {
    const [message, setMessage] = useState(`${currencyFrom.name} = 0.00 ${currencyTo.name}`);

    useEffect(() => {
        setMessage(`${currencyFrom.name} = ${convertWithFix(currencyFrom, currencyTo)} ${currencyTo.name}`)
    }, [ currencyFrom, currencyTo ]);

    return (
        <div className="conversionRate">
            <span>Conversion Rate:</span>
            <span>1 {message}</span>
        </div>
    );
}

export default ConversionRateComponent;