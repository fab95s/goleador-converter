import React, { useCallback } from "react";
import currencies, { goleador } from "../utils/currencies";

import "../styles/currencies.scss";

function CurrencyComponent({ id, currency, setCurrency }) {

    const handleChange = useCallback((e) => {
        const selectedValue = e.target.value;
        const selectedCurrency = currencies.find(el => el.iso === selectedValue);
        setCurrency(selectedCurrency);
    }, []);

    return (
        <>
            { currency.iso === goleador.iso
                ? <span id={id} className="currency">{goleador.name} {goleador.symbol}</span>
                : <select id={id} className="currency list" value={currency.iso} onChange={handleChange}>
                    { currencies.map((el, index) => <option key={index} value={el.iso}>{el.name} {el.symbol}</option>) }
                </select>
            }
        </>
    );
}

export default CurrencyComponent;