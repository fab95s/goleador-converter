import React, { createRef, useCallback, useEffect, useState } from "react";
import currencies, { convertAmount, goleador } from "../utils/currencies";
import ConversionRateComponent from "./ConversionRateComponent";
import CurrencyComponent from "./CurrencyComponent";

import "../styles/converter.scss";

function ConverterComponent() {
    const amountRef = createRef();
    const [currencyFrom, setCurrencyFrom] = useState(goleador);
    const [currencyTo, setCurrencyTo] = useState(currencies.at(0));
    const [amount, setAmount] = useState(0);
    const [convertedValue, setConvertedValue] = useState(0);

    const handleChange = useCallback(() => {
        const amountValue = parseInt(amountRef.current.value, 10) || 0;
        setAmount(amountValue);
        setConvertedValue(convertAmount(amountValue, currencyFrom, currencyTo));
    }, [ amountRef, currencyFrom, currencyTo ]);

    const handleClick = useCallback(() => {
        const tempCurrencyFrom = currencyFrom;
        const tempCurrencyTo = currencyTo;
        setCurrencyFrom(tempCurrencyTo);
        setCurrencyTo(tempCurrencyFrom);
    }, [ currencyFrom, currencyTo ]);

    useEffect(handleChange, [ currencyFrom, currencyTo ]);

    return (
        <div className="converter">
            <ConversionRateComponent currencyFrom={currencyFrom} currencyTo={currencyTo} />
            <div className="exchange">
                <p>
                    <label htmlFor="amount">Amount:</label>
                    <span className={currencyFrom.iso} data-symbol={currencyFrom.symbol}>
                        <input type="number" id="amount" min="0" value={amount} ref={amountRef} onChange={handleChange} />
                    </span>
                </p>
                <p>
                    <label htmlFor="from">From:</label>
                    <CurrencyComponent id="from" currency={currencyFrom} setCurrency={setCurrencyFrom} />
                </p>
                <button type="button" className="swapCurrencies" onClick={handleClick}>Swap</button>
                <p>
                    <label htmlFor="to">To:</label>
                    <CurrencyComponent id="to" currency={currencyTo} setCurrency={setCurrencyTo} />
                </p>
            </div>
            <p>{amount} {currencyFrom.iso} = {convertedValue} {currencyTo.iso}</p>
        </div>
    );
}

export default ConverterComponent;