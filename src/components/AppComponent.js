import React from "react";
import ConverterComponent from "./ConverterComponent";

import "../styles/main.scss";

function AppComponent() {
    return (
        <>
            <h1>G<span className="goal">⚽</span>leador Converter</h1>
            <ConverterComponent />
        </>
    );
}

export default AppComponent;